<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class EmailVerificationCodeFactory extends Factory
{
    /**
     * @return string
     */
    public function getModel(): string
    {
        return config('email-authentication.email_verification_model');
    }

    public function definition(): array
    {
        return [
            'code' => $this->faker->unique()->bothify('######'),
            'expires_on' => Carbon::now()->addSeconds(
                config('auth.verification.time_to_expire')
            ),
        ];
    }
}
