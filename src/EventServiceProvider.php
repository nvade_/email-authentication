<?php

namespace Nvade\EmailAuthentication;

use Nvade\EmailAuthentication\Events\AuthCodeGenerated;
use Nvade\EmailAuthentication\Events\AuthenticatedByEmail;
use Nvade\EmailAuthentication\Events\Registered;
use Nvade\EmailAuthentication\Listeners\GenerateAuthenticationCode;
use Nvade\EmailAuthentication\Listeners\InvalidateAuthenticationCodeWhenAuthenticated;
use Nvade\EmailAuthentication\Listeners\InvalidateAuthenticationCodeWhenExpired;
use Nvade\EmailAuthentication\Listeners\MarkEmailAsVerified;
use Nvade\EmailAuthentication\Listeners\SendAuthenticationEmail;
use Illuminate\Auth\Events\Verified;

class EventServiceProvider extends \Illuminate\Foundation\Support\Providers\EventServiceProvider
{

    /**
     * The event to listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [

        Registered::class => [
            GenerateAuthenticationCode::class,
        ],

        AuthCodeGenerated::class => [
            SendAuthenticationEmail::class,
            InvalidateAuthenticationCodeWhenExpired::class,
        ],

        Verified::class => [
            MarkEmailAsVerified::class,
        ],

        AuthenticatedByEmail::class => [
            InvalidateAuthenticationCodeWhenAuthenticated::class,
        ],
    ];

    /**
     * Determine if events and listeners should be automatically discovered.
     *
     * @return bool
     */
    public function shouldDiscoverEvents(): bool
    {
        return true;
    }
}
