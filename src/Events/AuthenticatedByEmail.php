<?php

namespace Nvade\EmailAuthentication\Events;

use Nvade\EmailAuthentication\Contracts\EmailAuthenticatable;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class AuthenticatedByEmail
{
    use Dispatchable;
    use SerializesModels;

    public function __construct(
        public string $guard,
        public EmailAuthenticatable $authenticatable
    ) {
    }
}
