<?php

namespace Nvade\EmailAuthentication\Events;

use Nvade\EmailAuthentication\Contracts\EmailAuthenticatable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class Registered
{
    use Dispatchable;
    use SerializesModels;

    public function __construct(
        public EmailAuthenticatable $authenticatable
    ) {
    }
}
