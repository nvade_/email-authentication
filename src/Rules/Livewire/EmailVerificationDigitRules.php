<?php

namespace Nvade\EmailAuthentication\Rules\Livewire;

trait EmailVerificationDigitRules
{
    public array $digits = [
        0 => null,
        1 => null,
        2 => null,
        3 => null,
        4 => null,
        5 => null,
    ];

    public function emailVerificationDigitRules(): array
    {
        $rule = [];
        for ($i = 0; $i < count($this->digits) - 1; $i++) {
            $rule[$i - 1] = ['required', 'numeric'];
        }
        return $rule;
    }

    public function emailVerificationCode(): string
    {
        return implode('', $this->digits);
    }
}
