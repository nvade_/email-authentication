<?php

namespace Nvade\EmailAuthentication;

use Illuminate\Auth\Notifications\VerifyEmail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Notifications\Messages\MailMessage;

class EmailAuthenticationServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/email-authentication.php', 'email-authentication');
        $this->mergeConfigFrom(__DIR__ . '/../config/auth.php', 'auth');

        $this->app->register(EventServiceProvider::class);
    }

    public function boot()
    {
        $this->configurePublishing();

        VerifyEmail::toMailUsing(function (object $notifiable, string $url) {
            /**
             * @var Model|Notifiable $notifiable
             */
            $notifiable->load('emailVerificationCode');

            $code = $notifiable->emailVerificationCode;

            return (new MailMessage())
                ->subject('Verify Email Address')
                ->line('Enter the following code')
                ->line($code);
        });
    }

    protected function configurePublishing()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__.'/../config/email-authentication.php' => config_path('email-authentication.php'),
                __DIR__.'/../config/auth.php' => config_path('auth.php'),
            ], 'email-authentication-config');

            $this->publishes([
                __DIR__.'/../database/migrations' => database_path('migrations'),
            ], 'email-authentication-migrations');
        }
    }
}
