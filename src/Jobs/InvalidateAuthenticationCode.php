<?php

namespace Nvade\EmailAuthentication\Jobs;

use Nvade\EmailAuthentication\Contracts\EmailAuthenticatable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class InvalidateAuthenticationCode implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public EmailAuthenticatable $authenticatable;

    public function __construct(EmailAuthenticatable $authenticatable)
    {
        $this->authenticatable = $authenticatable;
    }

    public function handle(): void
    {
        $this->authenticatable->invalidateAuthCode();
    }
}
