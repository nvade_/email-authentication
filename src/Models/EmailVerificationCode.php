<?php

namespace Nvade\EmailAuthentication\Models;

use Database\Factories\EmailVerificationCodeFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Stringable;

class EmailVerificationCode extends Model implements Stringable
{
    use SoftDeletes;
    use HasFactory;

    protected $fillable = [
        'code',
        'expires_on',
        'authenticatable_id',
        'authenticatable_type',
    ];

    protected $casts = [
        'expires_on' => 'datetime',
    ];

    public function authenticatable(): MorphTo
    {
        return $this->morphTo();
    }

    public function __toString(): string
    {
        return $this->code;
    }

    public function toArray(): array
    {
        return str_split($this->code, 1);
    }

    protected static function newFactory()
    {
        return EmailVerificationCodeFactory::new();
    }
}
