<?php

namespace Nvade\EmailAuthentication;

use Illuminate\Auth\EloquentUserProvider;
use Illuminate\Contracts\Hashing\Hasher as HasherContract;

class CentralUserProvider extends EloquentUserProvider
{

    /**
     * Create a new database user provider.
     *
     * @return void
     */
    public function __construct(HasherContract $hasher, string $model)
    {
        parent::__construct(
            $hasher,
            $model
        );
    }
}
