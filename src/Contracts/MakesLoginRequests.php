<?php

namespace Nvade\EmailAuthentication\Contracts;

use Illuminate\Http\RedirectResponse;

interface MakesLoginRequests
{
    public function attempt(string $email): RedirectResponse;
}
