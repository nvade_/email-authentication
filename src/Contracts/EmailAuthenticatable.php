<?php

namespace Nvade\EmailAuthentication\Contracts;

interface EmailAuthenticatable
{
    public function generateAuthCode(): bool;
    public function invalidateAuthCode(): void;
    public function verifyByAuthCode(string $code): bool;

    public static function findByEmail(string $email): ?EmailAuthenticatable;
}
