<?php

namespace Nvade\EmailAuthentication\Contracts;

use App\Models\User;

interface RegistersNewUsers
{
    public function register(string $email, callable $whenAlreadyExists = null): ?User;
}
