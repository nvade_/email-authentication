<?php

namespace Nvade\EmailAuthentication\Listeners;

use Nvade\EmailAuthentication\Events\AuthCodeGenerated;
use Illuminate\Contracts\Auth\MustVerifyEmail;

class SendAuthenticationEmail
{
    /**
     * Handle the event.
     *
     * @return void
     */
    public function handle(AuthCodeGenerated $event)
    {
        if ($event->for instanceof MustVerifyEmail && ! $event->for->hasVerifiedEmail()) {
            $event->for->sendEmailVerificationNotification();
        }
    }
}
