<?php

namespace Nvade\EmailAuthentication\Listeners;


use Nvade\EmailAuthentication\Events\AuthenticatedByEmail;

class InvalidateAuthenticationCodeWhenAuthenticated
{
    public function handle(AuthenticatedByEmail $event): void
    {
        if ($event->guard === config('email-authentication.guard')) {
            $event->authenticatable->invalidateAuthCode();
        }
    }
}
