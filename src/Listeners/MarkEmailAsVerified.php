<?php

namespace Nvade\EmailAuthentication\Listeners;

use Illuminate\Auth\Events\Verified;

class MarkEmailAsVerified
{
    public function __construct()
    {
    }

    public function handle(Verified $event): void
    {
        $event->user->markEmailAsVerified();
    }
}
