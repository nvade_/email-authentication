<?php

namespace Nvade\EmailAuthentication\Listeners;

use Nvade\EmailAuthentication\Events\AuthCodeGenerated;
use Nvade\EmailAuthentication\Events\Registered;
use Nvade\EmailAuthentication\Jobs\InvalidateAuthenticationCode;
use Illuminate\Contracts\Auth\MustVerifyEmail;

class InvalidateAuthenticationCodeWhenExpired
{
    /**
     * Handle the event.
     *
     * @return void
     */
    public function handle(AuthCodeGenerated $event)
    {
        InvalidateAuthenticationCode::dispatch($event->for)
            ->delay(now()->addSeconds(
                config('email-authentication.time_to_expire')
            ));
    }
}
