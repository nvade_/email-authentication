<?php

namespace Nvade\EmailAuthentication\Listeners;

use Nvade\EmailAuthentication\Events\Registered;

class GenerateAuthenticationCode
{
    /**
     * Handle the event.
     *
     * @return void
     */
    public function handle(Registered $event)
    {
        $event->authenticatable->generateAuthCode();
    }
}
