<?php

namespace Nvade\EmailAuthentication\Concerns;

use Illuminate\Database\Eloquent\Relations\MorphOne;
use Nvade\EmailAuthentication\Events\AuthCodeGenerated;
use Illuminate\Auth\Events\Authenticated;
use Illuminate\Auth\Events\Verified;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasOne;

trait EmailAuthenticatable
{
    public static function findByEmail(string $email): ?\Nvade\EmailAuthentication\Contracts\EmailAuthenticatable
    {
        /**
         * @var ?\Nvade\EmailAuthentication\Contracts\EmailAuthenticatable $result
         */
        $result = static::where('email', $email)->first();

        return $result;
    }

    public function generateAuthCode(): bool
    {
        if ($exists = $this->emailVerificationCode) {
            $this->invalidateAuthCode();
        }

        /**
         * @var HasFactory $model
         */
        $model = config('email-authentication.email_verification_model');

        $model::factory()->create([
            'authenticatable_id' => $this->id,
            'authenticatable_type' => __CLASS__
        ]);

        event(new AuthCodeGenerated($this));

        return true;
    }

    public function invalidateAuthCode(): void
    {
        $this->emailVerificationCode()?->delete();
    }

    /**
     * @return HasOne
     */
    public function emailVerificationCode(): MorphOne
    {
        return $this->morphOne(config('email-authentication.email_verification_model'), 'authenticatable')
            ->whereNull('email_verification_codes.deleted_at');
    }

    /**
     * Verifies the user's email verification code against the provided code. Marks the user's email as verified
     * on a successful match.
     *
     * @param string $code
     * @return bool
     */
    public function verifyByAuthCode(string $code): bool
    {
        if ($code === $this->emailVerificationCode->code) {
            if ($this->email_verified_at === null) {
                event(new Verified($this));
            }

            event(new Authenticated(config('email-authentication.guard'), $this));

            return true;
        }

        return false;
    }
}
