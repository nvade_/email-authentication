<?php

namespace Nvade\EmailAuthentication\Concerns;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\RedirectResponse;

trait MakesLoginRequests
{
    public function attempt(string $email): RedirectResponse
    {
        /**
         * @var \App\Auth\Contracts\EmailAuthenticatable $model
         */
        $model = config('email-authentication.email_verification_model');

        $user = $model::findByEmail($email);

        $user->generateAuthCode();

        return redirect()->route(config('email-authentication.routes.verify'))->with([
            'authenticatable' => $user
        ]);
    }
}
