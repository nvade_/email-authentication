<?php

namespace Nvade\EmailAuthentication\Concerns;

use Nvade\EmailAuthentication\Events\Registered;
use App\Models\User;
use App\Rules\Onboarding\EmailValidationRules;
use Illuminate\Auth\AuthenticationException;

trait RegistersNewUser
{
    use EmailValidationRules;

    /**
     * @param string        $email
     * @param callable|null $whenAlreadyExists Will be called with the existing user as parameter
     * @return User|null
     * @throws AuthenticationException When the specified email belongs to an existing user and no closure
     * to follow up was given.
     */
    public function register(string $email, callable $whenAlreadyExists = null): ?User
    {
        if ($exists = User::email($email)->first()) {
            if (!$whenAlreadyExists) {
                throw new AuthenticationException('Tried registering an existing user');
            }

            return $whenAlreadyExists($exists);
        }

        /**
         * @var \Nvade\EmailAuthentication\Contracts\EmailAuthenticatable $user
         */
        $user = User::create([
            'name' => $email,
            'email' => $email,
            'password' => bcrypt($email)
        ]);

        event(new Registered($user));

        return $user;
    }
}
