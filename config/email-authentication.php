<?php

return [

    'guard' => env('EMAIL_AUTH_GUARD', 'nvade'),

    'routes' => [
        'verify' => 'central.verify-email',
        'login' => 'central.login',
        'register' => 'central.register'
    ],

    'time_to_expire' => 3600,

    'email_verification_model' => \Nvade\EmailAuthentication\Models\EmailVerificationCode::class,

];
