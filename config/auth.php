<?php

use App\Auth\CentralUserProvider;

return [

    'guards' => [

        'nvade' => [
            'driver' => 'session',
            'provider' => 'users',
            'hash' => true,
        ],

    ],

    'passwords' => [

        'nvade' => [
            'provider' => 'users',
            'table' => 'password_resets',
            'expire' => 60,
            'throttle' => 60,
        ],

    ]

];
